import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Select from "react-select";

var options = [{ value: "one", label: "One" }, { value: "two", label: "Two" }];
const styles = {
  typo: {
    paddingLeft: "25%",
    marginBottom: "40px",
    position: "relative"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);
var props = {
  nom: "",
  password: ""
};
var seConnecter = true;

const styleButton = {
  height: "white",
  backgroundColor: "#fff",
  padding: "10px",
  fontFamily: "Arial",
  marginBottom: "1em",
  marginTop: "1em"
};

const listeValue = [
  { value: "PHP 7", label: "PHP 7" },
  { value: "Symphony", label: "Symphony 5" },
  { value: "Django", label: "Django" },
  { value: "Photoshop", label: "Photoshop" },
  { value: "IDesign", label: "IDesign" },
  { value: "UML", label: "UML" }
];

var listeValueSelected = [];

export default function LoginPage() {
  const classes = useStyles();
  const [sStrate, setLogin] = useState(seConnecter);
  const [selectedCompetence, setSelectedCompetence] = useState(
    listeValueSelected
  );

  const handleName = name => {
    props.nom = name;
    return 2;
  };

  const handlePassword = pass => {
    props.password = pass;
    return 2;
  };

  const handleChange = selectedOption => {
    console.log(selectedOption);
    setSelectedCompetence([...selectedCompetence, selectedOption]);
  };

  const handleSubmit = evt => {
    evt.preventDefault();
    console.log(props);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      email: props.nom,
      password: props.password
    });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow"
    };

    fetch("http://localhost:8000/login_check", requestOptions)
      .then(response => response.text())
      .then(result =>
        sessionStorage.setItem("token", "bearer " + JSON.parse(result)["token"])
      )
      .catch(error => console.log("error", error));
  };
  if (sStrate) {
    return (
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Se connecter</h4>
        </CardHeader>
        <CardBody>
          <div
            style={{
              display: "flex",
              justifyContent: "left",
              alignItems: "left"
              // height: "100vh"
            }}
          >
            <button type="submit" onClick={() => setLogin(!sStrate)}>
              Creer un compte
            </button>
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
              // height: "100vh"
            }}
          >
            <form onSubmit={handleSubmit}>
              <section className="text-input">
                <label>
                  <i className="fa fa-at" aria-hidden="true"></i>
                </label>
                <input
                  type="email"
                  placeholder="email"
                  size="40"
                  style={styleButton}
                  onChange={e => handleName(e.target.value)}
                />
              </section>
              <section className="text-input">
                <label>
                  <i className="fa fa-lock" aria-hidden="true"></i>
                </label>
                <input
                  type="password"
                  placeholder="password"
                  size="40"
                  style={styleButton}
                  onChange={e => handlePassword(e.target.value)}
                />
              </section>
              <section className="actions">
                <button type="submit">Login</button>
              </section>
            </form>
          </div>
        </CardBody>
      </Card>
    );
  } else {
    return (
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Se connecter</h4>
        </CardHeader>
        <CardBody>
          <div
            style={{
              display: "flex",
              justifyContent: "left",
              alignItems: "left"
              // height: "100vh"
            }}
          >
            <button type="submit" onClick={() => setLogin(!sStrate)}>
              Se connecter
            </button>
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
              // height: "100vh"
            }}
          >
            <form onSubmit={handleSubmit}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                  // height: "100vh"
                }}
              ></div>
              <section className="text-input">
                <label>
                  <i className="fa fa-user-circle-o" aria-hidden="true"></i>
                </label>
                <input
                  type="text"
                  placeholder="Nom"
                  size="40"
                  style={styleButton}
                  onChange={e => handleName(e.target.value)}
                />
              </section>

              <section className="text-input">
                <label>
                  <i className="fa fa-user-circle-o" aria-hidden="true"></i>
                </label>
                <input
                  type="text"
                  placeholder="Prenom"
                  size="40"
                  style={styleButton}
                  onChange={e => handleName(e.target.value)}
                />
              </section>

              <section className="text-input">
                <label>
                  <i className="fa fa-at" aria-hidden="true"></i>
                </label>
                <input
                  type="email"
                  placeholder="Email"
                  size="40"
                  style={styleButton}
                  onChange={e => handleName(e.target.value)}
                />
              </section>

              <section className="text-input">
                <label>
                  <i className="fa fa-lock" aria-hidden="true"></i>
                </label>
                <input
                  type="password"
                  placeholder="password"
                  size="40"
                  style={styleButton}
                  onChange={e => handlePassword(e.target.value)}
                />
              </section>
              <section className="text-input">
                <label>
                  <i className="fa fa-lock" aria-hidden="true"></i>
                </label>

                <Select
                  onChange={handleChange}
                  options={listeValue}
                  isMulti={true}
                />
              </section>
              <section className="text-input">
                <label>
                  <i className="fa fa-at" aria-hidden="true"></i>
                </label>
                <input
                  type="text"
                  placeholder="Portfolio"
                  size="40"
                  style={styleButton}
                  onChange={e => handleName(e.target.value)}
                />
              </section>

              <section className="text-input">
                <label htmlFor="files" className="btn">
                  Seletionner votre CV
                </label>
                <input
                  id="files"
                  style={{ visibility: "hidden" }}
                  type="file"
                />
              </section>
              <section className="text-input">
                <label htmlFor="files" className="btn">
                  Votre Video de motivation
                </label>
                <input
                  id="files"
                  style={{ visibility: "hidden" }}
                  type="file"
                />
              </section>

              {/* <input type="file" name="file" id="file" class="inputfile" />
<label for="file">Choose a file</label> */}
              <section className="actions">
                <button type="submit">Login</button>
              </section>
            </form>
          </div>
        </CardBody>
      </Card>
    );
  }
}
