import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Quote from "components/Typography/Quote.js";
import Muted from "components/Typography/Muted.js";
import Primary from "components/Typography/Primary.js";
import Info from "components/Typography/Info.js";
import Success from "components/Typography/Success.js";
import Warning from "components/Typography/Warning.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import * as Survey from "survey-react";
import "survey-react/survey.css";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";


const styles = {
  typo: {
    paddingLeft: "25%",
    marginBottom: "40px",
    position: "relative"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function TypographyPage() {
  const classes = useStyles();

  let json = {
    title: "Quizz sur l'hisoire d'amerique",
    showProgressBar: "bottom",
    showTimerPanel: "top",
    maxTimeToFinishPage: 10,
    maxTimeToFinish: 25,
    firstPageIsStarted: true,
    startSurveyText: "Commencer Quiz",
    pages: [
      {
        questions: [
          {
            type: "html",
            html:
              "Vous vous appretez a commencer un quizz sur l'histoire. <br/>Vous avez 10 seconde pour repondre à chaque question et 25 seconde for tout le quizz.<br/>Cliquez sur le bouton <b>'Start Quiz'</b>  quand vous etes pret."
          }
        ]
      },
      {
        questions: [
          {
            type: "radiogroup",
            name: "civilwar",
            title: "When was the Civil War?",
            choices: [
              "1750-1800",
              "1800-1850",
              "1850-1900",
              "1900-1950",
              "after 1950"
            ],
            correctAnswer: "1850-1900"
          }
        ]
      },
      {
        questions: [
          {
            type: "radiogroup",
            name: "libertyordeath",
            title: "Who said 'Give me liberty or give me death?'",
            choicesOrder: "random",
            choices: [
              "John Hancock",
              "James Madison",
              "Patrick Henry",
              "Samuel Adams"
            ],
            correctAnswer: "Patrick Henry"
          }
        ]
      },
      {
        maxTimeToFinish: 15,
        questions: [
          {
            type: "radiogroup",
            name: "magnacarta",
            title: "What is the Magna Carta?",
            choicesOrder: "random",
            choices: [
              "The foundation of the British parliamentary system",
              "The Great Seal of the monarchs of England",
              "The French Declaration of the Rights of Man",
              "The charter signed by the Pilgrims on the Mayflower"
            ],
            correctAnswer: "The foundation of the British parliamentary system"
          }
        ]
      }
    ],
    completedHtml:
      "<h4>You have answered correctly <b>{correctedAnswers}</b> questions from <b>{questionCount}</b>.</h4>"
  };
  // eslint-disable-next-line no-unused-vars
  function onComplete(survey, options) {
    //Write survey results into database
    console.log("Survey results: " + JSON.stringify(survey.data));
  }

  function showQuizz() {
    //Write survey results into database
    getQuizzz(!getQuizz);
  }


  const listeProjet = ["Histoire", "Symfony 5", "IDesign"];
  var afficher = false;
  var model = new Survey.Model(json);
  const [getQuizz, getQuizzz] = useState(afficher);
  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Material Dashboard Heading</h4>
        <p className={classes.cardCategoryWhite}>Questionnaire</p>
      </CardHeader>
      <CardBody>
        <Grid container spacing={1}>
          {listeProjet.map(projet => {
            return (
              // eslint-disable-next-line react/jsx-key
              <Grid item xs={4}>
                <Card>
                  <CardBody>
                    <CardContent>
                      <Typography variant="body2" component="p">
                        {projet}
                        <br />
                        <Button
                          variant="warning"
                          style={{ backgroundColor: "#fdc542" }}
                          onClick={showQuizz}
                        >
                          Lancer Quizz
                        </Button>
                      </Typography>
                    </CardContent>
                  </CardBody>
                </Card>
              </Grid>
            );
          })}
        </Grid>
        {getQuizz && <Survey.Survey model={model} onComplete={onComplete} />}
      </CardBody>
    </Card>
  );
}
