import React, { useState } from "react";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import { green } from "@material-ui/core/colors";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import FormGroup from "@material-ui/core/FormGroup";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import { Redirect } from "react-router-dom";


const useStyles = makeStyles(styles);

// eslint-disable-next-line no-unused-vars
var requestOptions = {
  method: "GET",
  redirect: "follow"
};

const routeChange = () => {
  return <Redirect to="/login" />;
};

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color="default" {...props} />);

export default function Dashboard() {
  const classes = useStyles();
  const classesCard = useStyles();
  const [listeProjet, setListeProjet] = useState([]);

  fetch("http://localhost:8000/api/projets", requestOptions)
    .then(response => response.text())
    .then(result => setListeProjet(JSON.parse(result)["hydra:member"]))
    .catch(error => console.log("error", error));

  const [state, setCheckbox] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true,
    checkedG: true
  });
  const handleChange = name => event => {
    setCheckbox({ ...state, [name]: event.target.checked });
  };
  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Projets clients</h4>
      </CardHeader>
      <CardContent>
        <FormGroup row>
          <FormControlLabel
            control={
              <GreenCheckbox
                checked={state.checkedG}
                onChange={handleChange("checkedG")}
                value="checkedG"
              />
            }
            label="Projets disponible"
          />
          <FormControlLabel
            control={
              <GreenCheckbox
                checked={false}
                onChange={handleChange("checkedG")}
                value="checkedG"
              />
            }
            label="Projets en cours"
          />
          <FormControlLabel
            control={
              <GreenCheckbox
                checked={false}
                onChange={handleChange("checkedG")}
                value="checkedG"
              />
            }
            label="Projets terminé"
          />
        </FormGroup>
        <Button
          variant="warning"
          style={{ backgroundColor: "#fdc542" }}
          onClick={routeChange}
        >
          Rejoignez-nous
        </Button>
        <Grid container spacing={1}>
          {listeProjet.map(projet => {
            return (
              // eslint-disable-next-line react/jsx-key
              <Grid item xs={4}>
                <Card className={classesCard.root} variant="outlined">
                  <CardContent>
                    <Typography variant="p" component="h4">
                      <b> {projet.nom} </b>
                    </Typography>
                    <Typography
                      className={classesCard.pos}
                      color="textSecondary"
                    >
                      remuneration : {projet.remunerationParDev} €
                      <br />
                      Nombre de point : {projet.nbPoints}
                    </Typography>
                    <Typography variant="body2" component="p">
                      {projet.description}
                      <br />
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small">Plus d'information</Button>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </CardContent>
    </Card>
  );
}
