import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Quote from "components/Typography/Quote.js";
import Muted from "components/Typography/Muted.js";
import Primary from "components/Typography/Primary.js";
import Info from "components/Typography/Info.js";
import Success from "components/Typography/Success.js";
import Warning from "components/Typography/Warning.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import confirme from "assets/img/Confirmé.png";
import expert from "assets/img/Expert.png";
import formateur from "assets/img/Formateur.png";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Grid from "@material-ui/core/Grid";

const styles = {
  typo: {
    paddingLeft: "25%",
    marginBottom: "40px",
    position: "relative"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function TypographyPage() {
  const classes = useStyles();
  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Information</h4>
      </CardHeader>
      <CardBody>
        <h2>Niveau de d'experience</h2>
        <h4>La plateform possede 3 niveaux d'experience</h4>
        <Grid container spacing={1}>
          <Grid item xs={4}>
            <img
              src={confirme}
              style={{
                width: "20%",
                height: "20%%",
                zIndex: 1,
                marginBottom: "10%"
              }}
              alt="..."
            />
            <p>
              Le niveau confirmer Dispnible lors de la validation de
              l'inscription
            </p>
          </Grid>
          <Grid item xs={4}>
            <img
              src={expert}
              style={{
                width: "20%",
                height: "20%%",
                zIndex: 1,
                marginBottom: "10%"
              }}
              alt="..."
            />
            <p>Le niveau expert disonible a <b>500 points</b> de projet</p>
          </Grid>
          <Grid item xs={4}>
            <img
              src={formateur}
              style={{
                width: "20%",
                height: "20%%",
                zIndex: 1,
                marginBottom: "10%"
              }}
              alt="..."
            />
            <p>
              Le niveau formateur donnant le droit de former de nouveaux membre
              diplonible a <b>1500 points</b>
            </p>
          </Grid>
        </Grid>
      </CardBody>
    </Card>
  );
}
